import { Routes } from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {WelcomeComponent} from "./components/welcome/welcome.component";
import {UsersComponent} from "./components/users/users.component";
import {AddUserComponent} from "./components/add-user/add-user.component";
import {EditUserComponent} from "./components/edit-user/edit-user.component";
import {authentificationGuard} from "./guards/authentification.guard";
import {authorizationGuard} from "./guards/authorization.guard";

export const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "welcome", component: WelcomeComponent, canActivate: [authentificationGuard]},
  {path: "addUser", component: AddUserComponent, canActivate: [authentificationGuard, authorizationGuard]},
  {path: "editUser/:id", component: EditUserComponent, canActivate: [authentificationGuard, authorizationGuard]},
  {path: "users", component: UsersComponent, canActivate: [authentificationGuard, authorizationGuard]},
  {path: "**", redirectTo: "/welcome"}
];
