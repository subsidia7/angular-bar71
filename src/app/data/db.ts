import {User} from "../domain/user";
import {Role} from "../domain/role";

export const db: User[] = [
  {
    id: 0,
    username: "spirit",
    password: "1234",
    email: "kheruvimovv@gmail.com",
    name: "Dmitriy",
    surname: "Kheruvimov",
    patronymic: "Konstantinovich",
    birthday: new Date("2001-05-29"),
    roles: [
      {
        id: 0,
        naming: "admin"
      },
      {
        id: 1,
        naming: "security"
      }
      ]
  },
  {
    id: 1,
    username: "jix",
    password: "1111",
    email: "sc.janka@rambler.ru",
    name: "Yan",
    surname: "Stepanov",
    patronymic: "Vadimovich",
    birthday: new Date("2000-07-06"),
    roles: [
      {
        id: 2,
        naming: "manager"
      },
      {
        id: 1,
        naming: "security"
      },
      {
        id: 3,
        naming: "tester"
      }
    ]
  }
]

export const roles: Role[] = [
  {
    id: 2,
    naming: "manager"
  },
  {
    id: 1,
      naming: "security"
  },
  {
    id: 3,
      naming: "tester"
  },
  {
    id: 0,
      naming: "admin"
  },
  {
    id: 4,
      naming: "director"
  }
];


