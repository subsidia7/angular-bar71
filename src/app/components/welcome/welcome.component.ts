import {Component, inject} from '@angular/core';
import {NavbarComponent} from "../navbar/navbar.component";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-welcome',
  standalone: true,
  imports: [
    NavbarComponent
  ],
  templateUrl: './welcome.component.html',
  styleUrl: './welcome.component.css'
})
export class WelcomeComponent {
  userService = inject(UserService);
}
