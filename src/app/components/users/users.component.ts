import {Component, inject} from '@angular/core';
import {NavbarComponent} from "../navbar/navbar.component";
import {UserService} from "../../services/user.service";
import {JsonPipe, NgFor} from "@angular/common";
import {User} from "../../domain/user";
import {Router, RouterLink, RouterLinkActive} from "@angular/router";
import {TableModule} from "primeng/table";
import {ButtonModule} from "primeng/button";

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [
    NgFor,
    NavbarComponent,
    JsonPipe,
    RouterLink,
    RouterLinkActive,
    TableModule,
    ButtonModule,
  ],
  templateUrl: './users.component.html',
  styleUrl: './users.component.css'
})
export class UsersComponent {

  userService:UserService = inject(UserService);
  router:Router = inject(Router);
  db: User[] = this.userService.getAllUsers();

  constructor() {
    console.log(this.db);
  }


  submitDelete(id: number): void {
    this.userService.deleteUser(id);
    this.db = this.userService.getAllUsers();
    location.reload()
  }

}
