import {Component, inject, OnInit} from '@angular/core';
import {NavbarComponent} from "../navbar/navbar.component";
import {ReactiveFormsModule, Validators, FormBuilder, FormArray, FormGroup} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {NgFor, NgIf} from "@angular/common";
import {ActivatedRoute} from "@angular/router";
import {User} from "../../domain/user";
import {validateOneRoleRequired} from "../../validators/roles.validator";
import {validatePast} from "../../validators/birthday.validator";
import {CardModule} from "primeng/card";
import {CheckboxModule} from "primeng/checkbox";
import {ButtonModule} from "primeng/button";
import {InputTextModule} from "primeng/inputtext";
import {CalendarModule} from "primeng/calendar";

@Component({
  selector: 'app-add-user',
  standalone: true,
  imports: [
    NavbarComponent,
    ReactiveFormsModule,
    NgIf,
    NgFor,
    CardModule,
    CheckboxModule,
    ButtonModule,
    InputTextModule,
    CalendarModule
  ],
  templateUrl: './edit-user.component.html',
  styleUrl: './edit-user.component.css'
})
export class EditUserComponent{

  route: ActivatedRoute = inject(ActivatedRoute);
  formBuilder = inject(FormBuilder);
  userService = inject(UserService);
  allRoles = this.userService.getAllRoles();
  user: User | undefined = undefined;
  editUserForm: FormGroup;

  constructor() {
    const userId = parseInt(this.route.snapshot.params['id'], 10);
    this.user = this.userService.getUserById(userId);

    this.editUserForm = this.formBuilder.group({
        username: [this.user?.username, [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(20)]
        ],
        password: [this.user?.password, [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(14)]
        ],
        email: [this.user?.email, [
          Validators.required,
          Validators.email],
        ],
        name: [this.user?.name, [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(25)]
        ],
        surname: [this.user?.surname, [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(25)]
        ],
        patronymic: [this.user?.patronymic, [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(25)]
        ],
        birthday: [new Date(this.user!.birthday), [
          Validators.required,
          validatePast()]
        ],
        roles: this.formBuilder.array([], validateOneRoleRequired())
      }
    )

    const fa: FormArray = this.editUserForm.get('roles') as FormArray;
    for (const role of this.allRoles) {
      fa.push(this.formBuilder.control(this.user?.roles.some(r => r.naming === role.naming)));
    }
  }

  get username() {
    return this.editUserForm.get('username');
  }

  get password() {
    return this.editUserForm.get('password');
  }

  get email() {
    return this.editUserForm.get('email');
  }

  get surname() {
    return this.editUserForm.get('surname');
  }

  get name() {
    return this.editUserForm.get('name');
  }

  get patronymic() {
    return this.editUserForm.get('patronymic');
  }

  get birthday() {
    return this.editUserForm.get('birthday');
  }

  get roles() {
    return this.editUserForm.get('roles') as FormArray;
  }

  submitEditUser(): void {
    console.log(this.allRoles);
    let userRoles = [];
    for (let i = 0; i < this.allRoles.length; i++) {
      if(this.roles.at(i).value) {
        userRoles.push(this.allRoles[i]);
      }
    }
    this.userService.editUser({
        id: -1,
        username: this.editUserForm.value.username!,
        password: this.editUserForm.value.password!,
        email: this.editUserForm.value.email!,
        name: this.editUserForm.value.name!,
        surname: this.editUserForm.value.surname!,
        patronymic: this.editUserForm.value.patronymic!,
        birthday: this.editUserForm.value.birthday!,
        roles: userRoles
      }
    );
  }

}
