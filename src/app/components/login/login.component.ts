import {Component, inject} from '@angular/core';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {NgIf} from "@angular/common";
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule, NgIf, CardModule, InputTextModule, FormsModule, ButtonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  userService: UserService = inject(UserService);
  router = inject(Router);

  loginForm = new FormGroup({
      username: new FormControl('spirit', Validators.required),
      password: new FormControl('1234', Validators.required)
    }
  )

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  submitAuth(): void {
    if(this.userService.login(this.loginForm.value.username!, this.loginForm.value.password!)) {
      this.router.navigate(['/welcome']);
    }
  }

}
