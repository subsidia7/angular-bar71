import {Component, inject, OnInit} from '@angular/core';
import {NavbarComponent} from "../navbar/navbar.component";
import {ReactiveFormsModule, Validators, FormBuilder, FormControl, FormArray} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {NgFor, NgIf} from "@angular/common";
import {validateOneRoleRequired} from "../../validators/roles.validator";
import {validatePast} from "../../validators/birthday.validator";
import { CheckboxModule } from 'primeng/checkbox';
import {InputTextModule} from "primeng/inputtext";
import { ButtonModule } from 'primeng/button';
import {CardModule} from "primeng/card";
import {CalendarModule} from "primeng/calendar";

@Component({
  selector: 'app-add-user',
  standalone: true,
  imports: [
    NavbarComponent,
    ReactiveFormsModule,
    NgIf,
    NgFor,
    CheckboxModule,
    InputTextModule,
    ButtonModule,
    CardModule,
    CalendarModule
  ],
  templateUrl: './add-user.component.html',
  styleUrl: './add-user.component.css'
})
export class AddUserComponent implements OnInit{

  formBuilder = inject(FormBuilder);
  userService = inject(UserService);
  allRoles = this.userService.getAllRoles();

  addUserForm = this.formBuilder.group({
      username: ['dolly', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)]
      ],
      password: ['qwerty', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(14)]
      ],
      email: ['jiji@mail.ru', [
        Validators.required,
        Validators.email],
      ],
      name: ['Kate', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)]
      ],
      surname: ['Jinger', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)]
      ],
      patronymic: ['Polyoly', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(25)]
      ],
      birthday: [new Date("2000-02-13"), [
        Validators.required,
        validatePast()]
      ],
      roles: this.formBuilder.array([], [validateOneRoleRequired()])
  }
  )
  ngOnInit() {
    const fa: FormArray = this.addUserForm.get('roles') as FormArray;
    for (const role of this.allRoles) {
      fa.push(this.formBuilder.control(false));
    }
    console.log(this.addUserForm.controls.roles)
  }

  get username() {
    return this.addUserForm.get('username');
  }

  get password() {
    return this.addUserForm.get('password');
  }

  get email() {
    return this.addUserForm.get('email');
  }

  get surname() {
    return this.addUserForm.get('surname');
  }

  get name() {
    return this.addUserForm.get('name');
  }

  get patronymic() {
    return this.addUserForm.get('patronymic');
  }

  get birthday() {
    return this.addUserForm.get('birthday');
  }

  get roles() {
    return this.addUserForm.get('roles') as FormArray;
  }

  submitAddUser(): void {
    console.log(this.allRoles);
    let userRoles = [];
    for (let i = 0; i < this.allRoles.length; i++) {
      if(this.roles.at(i).value) {
        userRoles.push(this.allRoles[i]);
      }
    }
    this.userService.addUser({
        id: -1,
        username: this.addUserForm.value.username!,
        password: this.addUserForm.value.password!,
        email: this.addUserForm.value.email!,
        name: this.addUserForm.value.name!,
        surname: this.addUserForm.value.surname!,
        patronymic: this.addUserForm.value.patronymic!,
        birthday: new Date(this.addUserForm.value.birthday!),
        roles: userRoles
      }
    );
  }

}
