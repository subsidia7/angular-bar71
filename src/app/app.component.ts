import {Component, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {Router, RouterLink, RouterLinkActive, RouterOutlet} from '@angular/router';
import {UserService} from "./services/user.service";
import {User} from "./domain/user";
import {ButtonModule} from "primeng/button";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, RouterLinkActive, RouterLink, ButtonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {

  userService = inject(UserService);
  router:Router = inject(Router);
  protected readonly sessionStorage = sessionStorage;

  logout(): void {
    this.userService.logout();
    this.router.navigate(['/login']);
  }


}
