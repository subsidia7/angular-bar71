import {CanActivateFn, Router, UrlTree} from '@angular/router';
import {User} from "../domain/user";
import {inject} from "@angular/core";
import {UserService} from "../services/user.service";

export const authorizationGuard: CanActivateFn = (route, state): boolean | UrlTree => {
  const json = sessionStorage.getItem("user")
  if (!json) {
    return inject(Router).createUrlTree(['/login']);
  }
  const user: User = JSON.parse(json);
  if (inject(UserService).isAdmin(user)) {
    return true;
  }
  return inject(Router).createUrlTree(['/welcome']);
};
