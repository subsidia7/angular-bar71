import {AbstractControl, FormArray, ValidationErrors, ValidatorFn} from "@angular/forms";

export function validateOneRoleRequired(): ValidatorFn {
  return (control: AbstractControl):ValidationErrors | null =>
  {
    let oneChecked: boolean = false;
    if (control instanceof FormArray) {
      for (let i = 0; i < control.length; i++) {
        if (control.at(i).value === true) {
          oneChecked = true;
          break;
        }
      }
    }
    return oneChecked ? null : { oneRoleRequired: { value: "Select min one role" } };
  }
}
