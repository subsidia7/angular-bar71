import {Injectable} from '@angular/core';
import {User} from "../domain/user";
import {db, roles} from "../data/db"
import {Role} from "../domain/role";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() {
    if (!sessionStorage.getItem("db")) {
      sessionStorage.setItem("db", JSON.stringify(db));
    }
  }

  login(username: string, password: string): boolean {
    const users: User[] = JSON.parse(sessionStorage.getItem("db")!) as User[];
    const user: User | undefined = users.find((user: User) => {
      return user.username === username && user.password === password;
    })
    if (user) {
      sessionStorage.setItem("user", JSON.stringify(user));
      return true;
    }
    return false;
  }

  logout(): void {
    sessionStorage.removeItem("user");
  }

  getSessionUsername(): string {
    const user = JSON.parse(sessionStorage.getItem("user")!) as User;
    return user.username;
  }

  getAllUsers(): User[] {
    return JSON.parse(sessionStorage.getItem("db")!) as User[];
  }

  addUser(user: User): void {
    console.log(user);
    console.log(JSON.stringify(user));
    const users: User[] = JSON.parse(sessionStorage.getItem("db")!) as User[];
    user.id = users[users.length - 1].id + 1;
    users.push(user);
    sessionStorage.setItem("db", JSON.stringify(users));
  }

  editUser(user: User): void {
    console.log(user);
    console.log(JSON.stringify(user));
    const users: User[] = JSON.parse(sessionStorage.getItem("db")!) as User[];
    users.splice(users.findIndex((item) => item.username === user.username), 1);
    user.id = users[users.length - 1].id + 1;
    users.push(user);
    sessionStorage.setItem("db", JSON.stringify(users));

  }

  deleteUser(id: number): void {
    console.log(id);
    const users: User[] = JSON.parse(sessionStorage.getItem("db")!) as User[];
    users.splice(users.findIndex((item) => item.id === id), 1);
    sessionStorage.setItem("db", JSON.stringify(users));
  }

  getUserById(id: number): User | undefined {
    console.log(id);
    const users: User[] = JSON.parse(sessionStorage.getItem("db")!) as User[];
    return users.find((item) => item.id === id);
  }

  getAllRoles(): Role[] {
    return roles;
  }

  isAdmin(user: User): boolean {
    if (user.roles.some(r => r.naming === "admin")) {
      return true;
    }
    return false;
  }

}
